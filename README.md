# openvpn-docker

This is the same as https://github.com/jfelten/openvpn-docker but it uses the latest release of
Alpine 3.

## Contents

There is not much here other than a base alpine image with packages needed to run openvpn: openssl, easy-rsa, openvpn, and iptables.  Much of the configuration is done through kubernetes and helm. Please refer to the scripts [here](https://github.com/kubernetes/charts/blob/master/stable/openvpn/templates/config-openvpn.yaml) for better understanding.

